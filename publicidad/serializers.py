from rest_framework.serializers import ModelSerializer

from publicidad.models import Publicidad


class PublicidadSerializer(ModelSerializer):
    class Meta:
        model = Publicidad
        exclude = []
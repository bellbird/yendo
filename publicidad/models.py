from django.db import models

# Create your models here.
class Publicidad(models.Model):
    nombre = models.CharField(max_length=100,null=True)
    imagen = models.ImageField(upload_to='publicidades/')
    fecha_inicio = models.DateTimeField(verbose_name='Fecha de inicio')
    fecha_fin = models.DateTimeField(verbose_name='Fecha de finalizacion')

    class Meta:
        verbose_name_plural= 'Publicidades'


from django.shortcuts import render

# Create your views here.
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

from publicidad.models import Publicidad
from publicidad.serializers import PublicidadSerializer


class PublicidadViewSet(ReadOnlyModelViewSet):
    serializer_class = PublicidadSerializer
    queryset = Publicidad.objects.all()


from django.contrib import admin
from .models import *
# Register your models here.
class PublicidadAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'fecha_inicio', 'fecha_fin']

admin.site.register(Publicidad,PublicidadAdmin)
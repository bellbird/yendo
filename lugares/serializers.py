from rest_framework.serializers import ModelSerializer

from lugares.models import Lugar, Promo, Sucursal, Hotel, Habitacion, Reserva

class HabitacionSerializer(ModelSerializer):
    class Meta:
        model = Habitacion
        exclude = ['hotel']

class ReservaSerializer(ModelSerializer):
    class Meta:
        model = Reserva
        exclude = []

class HotelSerializer(ModelSerializer):
    habitaciones= HabitacionSerializer(many=True, read_only=True)
    class Meta:
        model = Hotel
        fields = ('id', 'nombre', 'habitaciones')

class SucursalSerializer(ModelSerializer):

    class Meta:
        model = Sucursal
        exclude = ['lugar']

class PromoSerializer(ModelSerializer):
    class Meta:
        model = Promo
        exclude = []

class LugarSerializer(ModelSerializer):
    sucursales = SucursalSerializer(many=True, read_only=True)
    hoteles = HotelSerializer(many=True, read_only=True)
    promos = PromoSerializer(many=True, read_only=True)
    class Meta:
        model = Lugar
        fields = ('id', 'nombre', 'descripcion', 'logo', 'imagen_portada', 'votos', 'calificacion',
                 'categorias', 'tipo_cocinas', 'forma_pagos', 'servicios', 'sucursales', 'promos',
                 'ubicaciones_reserva', 'motivos', 'ambientaciones', 'hoteles')
        depth = 1


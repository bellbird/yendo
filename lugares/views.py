from django.shortcuts import render

# Create your views here.
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

from lugares.models import Lugar, Promo, Sucursal, Reserva
from lugares.serializers import LugarSerializer, PromoSerializer, SucursalSerializer, ReservaSerializer


class LugarViewSet(ReadOnlyModelViewSet):
    serializer_class = LugarSerializer
    queryset = Lugar.objects.all()

class PromoViewSet(ReadOnlyModelViewSet):
    serializer_class = PromoSerializer
    queryset = Promo.objects.all()

class SucursalViewSet(ReadOnlyModelViewSet):
    serializer_class = SucursalSerializer
    queryset = Sucursal.objects.all()

class ReservaViewSet(ModelViewSet):
    serializer_class = ReservaSerializer
    queryset = Reserva.objects.all()
# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-09 19:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lugares', '0010_auto_20170109_1835'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sucursal',
            name='ambientaciones',
        ),
        migrations.RemoveField(
            model_name='sucursal',
            name='hoteles',
        ),
        migrations.RemoveField(
            model_name='sucursal',
            name='motivos',
        ),
        migrations.RemoveField(
            model_name='sucursal',
            name='ubicaciones_reserva',
        ),
        migrations.AddField(
            model_name='lugar',
            name='ambientaciones',
            field=models.ManyToManyField(related_name='lugares', to='lugares.Ambientacion', verbose_name=b'Ambientaciones'),
        ),
        migrations.AddField(
            model_name='lugar',
            name='hoteles',
            field=models.ManyToManyField(related_name='lugares', to='lugares.Hotel', verbose_name=b'Hoteles'),
        ),
        migrations.AddField(
            model_name='lugar',
            name='motivos',
            field=models.ManyToManyField(related_name='lugares', to='lugares.Motivo', verbose_name=b'Motivos'),
        ),
        migrations.AddField(
            model_name='lugar',
            name='ubicaciones_reserva',
            field=models.ManyToManyField(related_name='lugares', to='lugares.UbicacionReserva', verbose_name=b'Ubicaciones de Reserva'),
        ),
    ]

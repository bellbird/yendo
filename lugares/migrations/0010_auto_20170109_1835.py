# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-09 18:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lugares', '0009_auto_20170109_1830'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sucursal',
            name='ambientacion',
        ),
        migrations.RemoveField(
            model_name='sucursal',
            name='hotel',
        ),
        migrations.RemoveField(
            model_name='sucursal',
            name='motivo',
        ),
        migrations.RemoveField(
            model_name='sucursal',
            name='ubicacion',
        ),
        migrations.AddField(
            model_name='sucursal',
            name='ambientaciones',
            field=models.ManyToManyField(related_name='sucursales', to='lugares.Ambientacion', verbose_name=b'Ambientaciones'),
        ),
        migrations.AddField(
            model_name='sucursal',
            name='hoteles',
            field=models.ManyToManyField(related_name='sucursales', to='lugares.Hotel', verbose_name=b'Hoteles'),
        ),
        migrations.AddField(
            model_name='sucursal',
            name='motivos',
            field=models.ManyToManyField(related_name='sucursales', to='lugares.Motivo', verbose_name=b'Motivos'),
        ),
        migrations.AddField(
            model_name='sucursal',
            name='ubicaciones_reserva',
            field=models.ManyToManyField(related_name='sucursales', to='lugares.UbicacionReserva', verbose_name=b'Ubicaciones de Reserva'),
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-02 17:42
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lugares', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sucursal',
            name='ubicacion',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='sucursal', to='generales.Ubicacion', verbose_name=b'Ubicaci\xc3\xb3n'),
        ),
    ]

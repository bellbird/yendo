# -*- coding: utf-8 -*-
from django.db import models


# Create your models here.
from generales.models import Ubicacion, Telefono, DiasSemana


def get_url_foto_locales(self, filename):
    return 'fotos/locales/{}/{}'.format(self.sucursal.lugar_id, filename)

class ActivoAbstract(models.Model):
    activo = models.BooleanField(default=False)

    class Meta:
        abstract=True

class Categoria(models.Model):  # Generales de config
    nombre = models.CharField(max_length=64, verbose_name='Categoria')

    class Meta:
        verbose_name_plural = "Categorias"

    def __str__(self):
        return "{}".format(self.nombre)

class TipoCocina(models.Model):  # Generales de config
    nombre = models.CharField(max_length=64, verbose_name='Nombre')

    class Meta:
        verbose_name_plural = "Tipo de cocinas"

    def __str__(self):
        return "{}".format(self.nombre)

class FormaPago(models.Model):  # Generales de config
    nombre = models.CharField(max_length=64, verbose_name='Nombre')

    class Meta:
        verbose_name_plural = "Formas de Pago"
        verbose_name = "Forma de Pago"

    def __str__(self):
        return "{}".format(self.nombre)

class Servicio(models.Model):  # Generales de config
    nombre = models.CharField(max_length=64, verbose_name='Nombre')

    class Meta:
        verbose_name_plural = "Servicios"

    def __str__(self):
        return "{}".format(self.nombre)

class UbicacionReserva (models.Model):
    nombre = models.CharField(max_length=64, verbose_name='Nombre')

    class Meta:
        verbose_name_plural = "UbicacionesReserva"

    def __str__(self):
        return "{}".format(self.nombre)

class Motivo (models.Model):
    nombre = models.CharField(max_length=64, verbose_name='Nombre')

    class Meta:
        verbose_name_plural = "Motivos"

    def __str__(self):
        return "{}".format(self.nombre)

class Ambientacion (models.Model):
    nombre = models.CharField(max_length=64, verbose_name='Nombre')

    class Meta:
        verbose_name_plural = "Ambientaciones"

    def __str__(self):
        return "{}".format(self.nombre)

class Hotel (models.Model):
    nombre = models.CharField(max_length=64, verbose_name='Nombre')
    class Meta:
        verbose_name_plural = "Hoteles"

    def __str__(self):
        return "{}".format(self.nombre)

class Habitacion (models.Model):
    nombre = models.CharField(max_length=64, verbose_name='Nombre')
    hotel = models.ForeignKey(Hotel, on_delete=models.PROTECT, related_name='habitaciones')
    class Meta:
        verbose_name_plural = "Habitaciones"

    def __str__(self):
        return "{}".format(self.nombre)

class Lugar(ActivoAbstract):
    nombre = models.CharField(max_length=64)
    descripcion = models.TextField(max_length=500)
    logo = models.ImageField(upload_to='lugares/logo/')
    imagen_portada = models.ImageField(null=True,upload_to='lugares/portada/')
    categorias = models.ManyToManyField(Categoria, related_name='lugares', verbose_name='Categorias')
    tipo_cocinas = models.ManyToManyField(TipoCocina, related_name='lugares', verbose_name='Tipo Cocinas')
    forma_pagos = models.ManyToManyField(FormaPago, related_name='lugares', verbose_name='Formas de pago')
    servicios = models.ManyToManyField(Servicio, related_name='lugares', verbose_name='Servicios')
    votos = models.IntegerField(default=0)
    calificacion = models.FloatField(default=1, max_length=5)
    # reserva
    ubicaciones_reserva = models.ManyToManyField(UbicacionReserva, verbose_name='Ubicaciones de Reserva',
                                                 related_name='lugares')
    motivos = models.ManyToManyField(Motivo, verbose_name='Motivos', related_name='lugares')
    ambientaciones = models.ManyToManyField(Ambientacion, verbose_name='Ambientaciones', related_name='lugares')
    hoteles = models.ManyToManyField(Hotel, verbose_name='Hoteles', related_name='lugares')
    class Meta:
        verbose_name_plural = "Lugares"

    def __str__(self):
        return "{}".format(self.nombre)

class Sucursal(ActivoAbstract):
    lugar = models.ForeignKey(Lugar, on_delete=models.PROTECT, related_name='sucursales')
    nombre = models.CharField(max_length=64)
    direccion = models.TextField(max_length=500, null=True)
    latitud = models.DecimalField(max_digits=11, decimal_places=8, null=True)
    longitud = models.DecimalField(max_digits=11, decimal_places=8, null=True)
    telefono = models.CharField(max_length=14)
    zona = models.CharField(max_length=64, null=True)
    telefono_relacion = models.ForeignKey(Telefono, on_delete=models.PROTECT, related_name='sucursal', verbose_name='Telefono',null=True,blank=True)
    hora_apertura = models.TimeField(default='00:00')
    hora_cierre = models.TimeField(default='00:00')



    class Meta:
        verbose_name_plural = "Sucursales"

    def __str__(self):
        return "{}".format(self.nombre)

class GaleriaLocales(models.Model):
    foto = models.ImageField(verbose_name='Foto de Local', upload_to=get_url_foto_locales)
    sucursal = models.ForeignKey(Sucursal, on_delete=models.PROTECT)

SECCIONES = (
    ('hoy', 'Que hay hoy?'),
    ('bebida', 'Donde tomamos hoy?'),
    ('comida', 'Donde comemos hoy?'),
    ('ahorro', 'Donde ahorramos hoy?'),
    ('ayer', 'Que paso ayer?'),
    ('nuestro', 'Lo nuestro'),
)

# Desde aca comienza la parte de promos
class Promo(ActivoAbstract):
    local = models.ForeignKey(Lugar, related_name='promos',null=True)
    nombre = models.CharField(max_length=64)
    descripcion = models.TextField(max_length=500)
    imagen = models.ImageField()
    fecha_hora_inicio = models.DateTimeField(null=True)
    fecha_hora_fin = models.DateTimeField(null=True)
    seccion = models.CharField(max_length=7,choices=SECCIONES,null=True)

    class Meta:
        verbose_name_plural = "Promociones"

    def __str__(self):
        return "{}".format(self.nombre)

class Reserva (models.Model):
    cantidad_personas = models.IntegerField(default=0)
    nombre = models.CharField(default='', max_length=64)
    numero = models.CharField(max_length=14)
    ubicacion = models.ForeignKey(UbicacionReserva, on_delete=models.PROTECT, related_name='reservas')
    motivo = models.ForeignKey(Motivo, on_delete=models.PROTECT, related_name='reservas')
    ambientacion = models.ForeignKey(Ambientacion, on_delete=models.PROTECT, related_name='reservas')
    fecha = models.DateField(null=True)
    horario = models.TimeField()
    sucursal = models.ForeignKey(Sucursal, on_delete=models.PROTECT, related_name='reservas')
    hotel = models.ForeignKey(Hotel, on_delete=models.PROTECT, related_name='reservas', blank=True, null=True)
    habitacion = models.ForeignKey(Habitacion, on_delete=models.PROTECT, related_name='reservas', blank=True, null=True)






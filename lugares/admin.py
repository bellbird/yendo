from django.contrib import admin
from .models import *


class SucursalInline(admin.StackedInline):
    model = Sucursal
    exclude = ['telefono_relacion','ubicacion']
    extra = 1


class PromoInline(admin.StackedInline):
    model = Promo
    extra = 1



class LugarAdmin(admin.ModelAdmin):
    inlines = [
        SucursalInline,
        PromoInline
    ]
    exclude =[]
    list_display = ['nombre']

class PromoAdmin(admin.ModelAdmin):
    list_display = ['nombre','descripcion','local','fecha_hora_inicio','fecha_hora_fin']

admin.site.register(Lugar, LugarAdmin)
admin.site.register(Categoria)
admin.site.register(Servicio)
admin.site.register(TipoCocina)
admin.site.register(FormaPago)
admin.site.register(Promo, PromoAdmin)
admin.site.register(Reserva)
admin.site.register(Sucursal)
admin.site.register(UbicacionReserva)
admin.site.register(Motivo)
admin.site.register(Habitacion)
admin.site.register(Hotel)
admin.site.register(Ambientacion)



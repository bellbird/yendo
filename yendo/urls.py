"""Yendo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.static import serve
from rest_framework import routers

from eventos.views import EventoViewSet, EventosMesViewSet
from lugares.views import LugarViewSet, PromoViewSet, ReservaViewSet
from publicidad.views import PublicidadViewSet
from yendo import settings

router = routers.DefaultRouter()
router.register(r'lugares', LugarViewSet, base_name='lugar')
router.register(r'promociones', PromoViewSet)
router.register(r'publicidad', PublicidadViewSet)
router.register(r'eventos', EventoViewSet)
router.register(r'eventos_mes', EventosMesViewSet, base_name='evento_mes')
router.register(r'reservas', ReservaViewSet)


urlpatterns = [
    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT}),
    url(r'^', admin.site.urls),
    url(r'^admin/', admin.site.urls),
    url(r'^usuarios/', include('usuarios.urls')),
    url(r'^api/', include(router.urls)),

]
urlpatterns += staticfiles_urlpatterns()

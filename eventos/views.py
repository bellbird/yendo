from yendo import settings
# Create your views here.
from rest_framework.viewsets import ReadOnlyModelViewSet, ViewSet

from eventos.models import Evento
from eventos.serializers import EventoSerializer, EventoMesSerializer
from rest_framework.views import Response


class EventoViewSet(ReadOnlyModelViewSet):
    serializer_class = EventoSerializer
    queryset = Evento.objects.all()

class EventosMesViewSet (ViewSet):


    def list(self, request, format=None):
        eventos = []
        if (request.query_params):
            mes = request.query_params.get('mes')
            anho = request.query_params.get('anho')
            for evento in Evento.objects.filter(fecha_inicio__year__gte=anho,
                                                fecha_inicio__month__gte=mes,
                                                fecha_fin__year__lte=anho,
                                                fecha_fin__month__lte=mes):

                eventos.append(EventoMesSerializer(evento).data)

        return Response(eventos)


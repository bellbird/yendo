from django.contrib import admin
from .models import *
# Register your models here.
class EventoAdmin(admin.ModelAdmin):
    list_display = ['nombre','descripcion','fecha_inicio','fecha_fin']
admin.site.register(Evento, EventoAdmin)
from rest_framework.serializers import ModelSerializer, CharField, ImageField

from eventos.models import Evento


class EventoSerializer(ModelSerializer):
    lugar_nombre = CharField(source='lugar.nombre', read_only=True)
    sucursal_zona = CharField(source='sucursal.zona', read_only=True)
    lugar_logo = ImageField(source='lugar.logo', read_only=True)

    class Meta:
        model = Evento
        fields = ['id','nombre','descripcion', 'imagen','fecha_inicio','fecha_fin','lugar_logo', 'lugar_nombre','sucursal_zona']

class EventoMesSerializer(ModelSerializer):
    class Meta:
        model = Evento
        exclude = ['lugar','sucursal']
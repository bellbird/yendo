from django.db import models

# Create your models here.
from lugares.models import Lugar, Sucursal


def get_url_foto_evento(self, filename):
    return 'fotos/eventos/{}/{}'.format(self.sucursal.lugar_id, filename)


class Evento(models.Model):
    nombre = models.CharField(max_length=64)
    descripcion = models.TextField(max_length=500)
    imagen = models.ImageField(upload_to='eventos/')
    fecha_inicio = models.DateTimeField(null=True)
    fecha_fin = models.DateTimeField(null=True)
    lugar = models.ForeignKey(Lugar, null=True)
    sucursal = models.ForeignKey(Sucursal, null=True, blank=True)

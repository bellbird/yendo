from django.db import models


# Create your models here.


class Telefono(models.Model):  # Modelo que puede ser general de uso
    tipo = models.CharField(max_length=64)
    numero = models.CharField(max_length=24)


class Ubicacion(models.Model):  # Modelo que puede ser general de uso
    latitud = models.CharField(max_length=64)
    longitud = models.CharField(max_length=64)

class DiasSemana(models.Model):
    nombre_corto = models.CharField(max_length=3)
    nombre_largo = models.CharField(max_length=9)

    def __str__(self):
        return "{}".format(self.nombre_largo)

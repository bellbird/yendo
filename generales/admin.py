from django.contrib import admin

# Register your models here.
from generales.models import DiasSemana

admin.site.register(DiasSemana)
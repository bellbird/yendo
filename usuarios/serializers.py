# -*- coding: utf-8 -*-
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.template.base import Token
from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _
from .models import UserProfile


class RegisterSerializer(serializers.Serializer):
    username = serializers.CharField(label=_("username"))
    email = serializers.CharField(label=_("email"))
    first_name = serializers.CharField(label=_("first_name"))
    last_name = serializers.CharField(label=_("last_name"))
    sexo = serializers.ChoiceField(label=_("sexo"),choices=UserProfile.SEXO_OPTIONS)
    fecha_nacimiento = serializers.DateField(label=_("Fecha de nacimiento"))
    password = serializers.CharField(label=_("Contraseña"), style={'input_type': 'password'})
    password2 = serializers.CharField(label=_("Repetir contraseña"), style={'input_type': 'password'})

    def validate(self, attrs):
        username = attrs.get('username')
        email = attrs.get('email')
        password = attrs.get('password')
        password2 = attrs.get('password2')
        try:
            User.objects.get(email=email)
            msg = _('Este mail ya ha sido utilizado.')
            print(msg)
            raise serializers.ValidationError(msg, code='authorization')
        except:
            pass

        try:
            User.objects.get(username=username)
            msg = _('Este nombre de usuario ya ha sido utilizado.')
            print(msg)
            raise serializers.ValidationError(msg, code='authorization')
        except:
            pass

        if password != password2:
            msg = _('Las contraseñas no coinciden.')
            print(msg)
            raise serializers.ValidationError(msg, code='authorization')

        return super().validate(attrs)



class AuthTokenSerializer(serializers.Serializer):
    email = serializers.CharField(label=_("email"))
    password = serializers.CharField(label=_("Password"), style={'input_type': 'password'})

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        try:
            username = User.objects.get(email=email).username
        except:
            msg = _('Este usuario no existe.')
            print(msg)
            raise serializers.ValidationError(msg, code='authorization')

        if username and password:
            print(username+" "+password)
            user = authenticate(username=username, password=password)

            if user:
                # From Django 1.10 onwards the `authenticate` call simply
                # returns `None` for is_active=False users.
                # (Assuming the default `ModelBackend` authentication backend.)
                if not user.is_active:
                    msg = _('User account is disabled.')
                    print(msg)
                    raise serializers.ValidationError(msg, code='authorization')
            else:
                msg = _('Unable to log in with provided credentials.')
                print(msg)
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = _('Must include "email" and "password".')
            print(msg)
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs

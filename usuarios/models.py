from django.contrib.auth.models import AbstractUser
from django.db import models
from django.contrib.auth.models import User

# Create your models here.
TIPO_ADMIN = 'A'
TIPO_HOTEL = 'H'
TIPO_COMUN = 'C'

class UserProfile(models.Model):
    SEXO_MASCULINO = 'M'
    SEXO_FEMENINO = 'F'
    SEXO_OPTIONS = (
        (SEXO_MASCULINO, 'Masculino'),
        (SEXO_FEMENINO, 'Femenino')
    )
    TIPOS_USUARIOS = (
        (TIPO_ADMIN,'Admin'),
        (TIPO_HOTEL, 'Hotel'),
        (TIPO_COMUN, 'Comun'),
    )
    tipo = models.CharField(max_length=1,choices=TIPOS_USUARIOS, default=TIPO_COMUN)
    usuario = models.OneToOneField(User, on_delete=models.CASCADE, related_name='perfil_usuario')
    sexo = models.CharField(max_length=1, choices=SEXO_OPTIONS, default=SEXO_MASCULINO)
    documento_identidad = models.CharField(max_length=24, null=True)
    telefono_principal = models.CharField(max_length=24, null=True)
    fecha_nacimiento = models.DateField()

    class Meta:
        verbose_name = 'Perfil de usuario'
        verbose_name_plural = 'Perfiles de usuario'

    def __str__(self):
        return 'Perfil de {}'.format(self.usuario)


class HotelProfile(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE, related_name='perfil_hotel')
    nombre_hotel = models.CharField(max_length=150)
    ruc = models.CharField(max_length=24,null=True)
    telefono_principal = models.CharField(max_length=24,null=True)

    class Meta:
        verbose_name = 'Perfil de usuario Hotel'
        verbose_name_plural = 'Perfiles de usuario Hotel'


    def __str__(self):
        return 'Perfil del hotel {}'.format(self.nombre_hotel)

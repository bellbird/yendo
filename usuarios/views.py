from django.contrib.auth.models import User
from rest_framework import parsers, renderers
from rest_framework import permissions
from rest_framework.authtoken.models import Token

from usuarios.models import UserProfile
from .serializers import AuthTokenSerializer, RegisterSerializer
from rest_framework.response import Response
from rest_framework.views import APIView

class Register(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        print(request.data)
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        username = serializer.validated_data['username']
        email = serializer.validated_data['email']
        password = serializer.validated_data['password']
        sexo = serializer.validated_data['sexo']
        fecha_nacimiento = serializer.validated_data['fecha_nacimiento']
        first_name = serializer.validated_data['first_name']
        last_name = serializer.validated_data['last_name']

        user = User.objects.create_user(username,email,password)
        user.first_name = first_name
        user.last_name = last_name
        user.save()
        userProfile = UserProfile()
        userProfile.usuario = user
        userProfile.fecha_nacimiento = fecha_nacimiento
        userProfile.sexo = sexo
        userProfile.save()

        token, created = Token.objects.get_or_create(user=user)
        return Response(
            {'token': token.key,
             'username':user.username,
             'first_name':user.first_name,
             'last_name':user.last_name,
             'email':user.email,
             'sexo':userProfile.sexo,
             'fecha_nacimiento':userProfile.fecha_nacimiento
             }
        )


class VerificarToken(APIView):
    throttle_classes = ()
    permission_classes = (permissions.IsAuthenticated,)
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)

    def get(self, request, *args, **kwargs):
        token, created = Token.objects.get_or_create(user=request.user)
        user = request.user
        return Response(
            {'token': token.key,
             'username':user.username,
             'first_name':user.first_name,
             'last_name':user.last_name,
             'email':user.email,
             'sexo':user.perfil_usuario.sexo,
             'fecha_nacimiento': user.perfil_usuario.fecha_nacimiento,
             }
        )

class ObtainAuthToken(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response(
            {'token': token.key,
             'username':user.username,
             'first_name':user.first_name,
             'last_name':user.last_name,
             'email':user.email,
             'sexo':user.perfil_usuario.sexo,
             'fecha_nacimiento': user.perfil_usuario.fecha_nacimiento,

             }
        )


/**
 * Created by rauleburro on 20/11/16.
 */

django.jQuery(document).ready(function () {
  var mostrarFormset = function () {
    if (django.jQuery('#id_is_hotel').is(':checked')) {
      django.jQuery('#perfil_usuario-group').hide();
      django.jQuery('#perfil_hotel-group').show();
    } else {
      django.jQuery('#perfil_usuario-group').show();
      django.jQuery('#perfil_hotel-group').hide();
    }
  };
  mostrarFormset();
  django.jQuery('#id_is_hotel').change(mostrarFormset);
});
from django.conf.urls import url
from .views import ObtainAuthToken,Register,VerificarToken


urlpatterns = [
    url(r'^api-token-auth/', ObtainAuthToken.as_view()),
    url(r'^registrar/', Register.as_view()),
    url(r'^verificar-token/', VerificarToken.as_view()),

]
